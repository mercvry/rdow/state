require_relative 'main.rb'
require_relative '../test/main.rb'
require "pastel"

pastel = Pastel.new

machine = RDow::StateMachine.new :clean do |m|
  m.when(:confirm, :clean     => :confirmed)
  m.when(:ignore,  :clean     => :ignored)
  m.when(:reset,   :confirmed => :clean,
                   :ignored   => :clean)

  m.on(:confirmed) { puts "confirmed" }
  m.on(:ignored)   { || puts "ignored" }
  m.on(:"*")       { |event| ! -> { "called event \"#{event}\"" } }
end

+-> do
  machine.confirm!
  machine.reset!
  machine.ignore!
  machine.reset!
  machine.confirm!
  machine.ignore!
end