module RDow
	class StateMachine
		InvalidEvent = Class.new(NoMethodError)
		InvalidState = Class.new(ArgumentError)

		attr_reader :transitions, :state

		def initialize(initial_state, &block)
			@state = initial_state
			@transitions = Hash.new
			@callbacks = Hash.new { |hash, key| hash[key] = [] }
			
			unless block.nil?
				block.call(self)
				self.freeze
			end
		end

		def on(event, &callback)
			@callbacks[event] << callback
		end

		def when(event, transitions)
			@transitions[event] = transitions
		end

		def trigger(event, data = nil)
			change(event, data) if trigger?(event)
		end

		def trigger!(event, data = nil)
			raise InvalidState.new("event \"#{event}\" is not valid for state \"#{@state}\"") unless trigger?(event)
			trigger(event, data)
		end

		def trigger?(event)
			raise InvalidEvent.new("event \"#{event}\" doesn't exist in the state machine") unless @transitions.has_key?(event)
			@transitions[event].has_key?(@state)
		end

		def events
			@transitions.keys
		end

		def triggerable_events
			events.select { |event| trigger?(event) }
		end

		def states
			@transitions.values.map(&:to_a).flatten.uniq
		end

		def freeze
			events.each do |event|
				define_singleton_method("#{event}".to_sym) do | *data |
					trigger(event, data)
				end
				define_singleton_method("#{event}?".to_sym) do
					trigger?(event)
				end
				define_singleton_method("#{event}!".to_sym) do | *data |
					trigger!(event, data)
				end
			end
			states.each do |state|
				define_singleton_method("#{state}?".to_sym) do
					return @state.equal? state
				end
			end
		end

		private

		def change(event, data = nil)
			@state = @transitions[event][@state]
			callbacks =  @callbacks[:"*"] + @callbacks[@state]
			callbacks.each { |callback| callback.call(event, *data) }
			true
		end
	end
end

