**RDow::StateMachine** is a dead simple finite state machine

#### features
- small implementation (63 SLOC)
- callbacks
- wildcard callbacks

#### usage
```ruby
machine = RDow::StateMachine.new :clean do |m|
	m.when :confirm, :clean     => :confirmed
	m.when :ignore,  :clean     => :ignored
	m.when :reset,   :confirmed => :clean,
	                 :ignored   => :clean
end

machine.confirm         #=> true
machine.ignore?         #=> false
machine.ignore!         # Raises RDow::StateMachine::InvalidState
```

#### todo
- [ ] add callbacks that are fired only once
- [x] freezeable machine (done by default)