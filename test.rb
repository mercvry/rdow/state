require_relative 'main.rb'
require_relative '../test/main.rb'

sm = RDow::StateMachine.new :clean do |_|
	_.when :throw, :clean => :dirty
	_.when :wipe,  :dirty => :clean

	_.on :"*" do |event, *arguments|
		!-> { "#{event} #{arguments}" }
	end
end

+-> do
	sm.throw!
	sm.wipe!
	puts
	'state transition'
end

--> do
	sm.wipe!
end

+-> do
	raise "states don't match" unless sm.clean?
	"states match"
end

!-> { puts; "finished with #{RDow::Test.count} successful tasks" }